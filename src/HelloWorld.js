import React, { Component } from 'react'
import PropTypes from 'prop-types';

class HelloWorld extends Component {
   render() {
       return (
           <div>
               <h1>{this.props.name}</h1>   
           </div>
       );
   }
}

HelloWorld.propTypes = {
    name: PropTypes.string.isRequired
};
export default HelloWorld;