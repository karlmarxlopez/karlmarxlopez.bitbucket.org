import React from 'react';
import {
    Step,
    Stepper,
    StepLabel,
    StepContent
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';

const PlayerEditSteps = ({finished, stepIndex, renderStepActions, editingFinished}) => {
    return (
        <Stepper activeStep={stepIndex} orientation="vertical">
            <Step>
                <StepLabel>Profile Info</StepLabel>
                <StepContent>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates veritatis voluptatem sunt facilis soluta quia? Nostrum voluptate nam velit beatae nihil, tempora ducimus ea maxime harum nulla nesciunt laboriosam!12</p>
                    {renderStepActions(0)}
                </StepContent>
            </Step>
            <Step>
                <StepLabel>Offerings</StepLabel>
                <StepContent>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates veritatis voluptatem sunt facilis soluta quia? Nostrum voluptate nam velit beatae nihil, tempora ducimus ea maxime harum nulla nesciunt laboriosam!12</p>
                    {renderStepActions(1)}
                </StepContent>
            </Step>
            <Step>
                <StepLabel>Actions</StepLabel>
                <StepContent>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptates veritatis voluptatem sunt facilis soluta quia? Nostrum voluptate nam velit beatae nihil, tempora ducimus ea maxime harum nulla nesciunt laboriosam!12</p>
                    {renderStepActions(2)}
                </StepContent>
            </Step>
        </Stepper>
    );
};

export default PlayerEditSteps;