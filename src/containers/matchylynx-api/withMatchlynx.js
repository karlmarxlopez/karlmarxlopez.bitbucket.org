import React, { Component } from 'react';
import MatchlynxAPI from './APIMethods';


const WithMatchlynx = (WrappedComponent) => {
    const MatchlynxAPIWrapper = class extends Component {
        constructor() {
            super();

            this.state = {
                platformName: '',
                platforms: [],
                players: [],
                actions: [],
                activePlayers: [],
                playerActions: {}
            };

            // this.getPlatformName = this.getPlatformName.bind(this);
            // this.getUserPlatforms = this.getUserPlatforms.bind(this);
            // this.getPlatformPlayers = this.getPlatformPlayers.bind(this);
            // this.getPlayerActions = this.getPlayerActions.bind(this);
        }
        getPlatformName = (platformID) => {
            
            // if (typeof platformID === 'number') {
            //     const platform = this.state.platforms.find(i => i.id === platformID)
            //     console.log(this.state.platforms);
            //     return new Promise((resolve, reject) => {
            //         resolve(platform.text)
            //     });
            // }
            MatchlynxAPI.getPlatformName(platformID)
                .then(platformName => this.setState({ platformName }));
        }

        getUserPlatforms = (profileID) => {
            console.log(this.state.platforms);
            return new Promise((resolve, reject) => {
                resolve(this.state.platforms);
            });

            MatchlynxAPI.getUserPlatforms(profileID)
                .then(platforms => this.setState({ platforms }));
        }

        getPlatformPlayers = (platformID) => {
            // const existing = this.state.players.filter(player => player.platform_id == platformID)
            // console.log(existing);
            MatchlynxAPI.getPlatformPlayers(platformID)
                .then(players => this.setState({ activePlayers: players }));
        }

        getPlayerActions = (playerID) => {
            const { playerActions } = this.state
            if (playerActions[playerID]) {
                return playerActions[playerID]
            } else {
                return MatchlynxAPI.getPlayerActions(playerID)
                    .then((actions = []) => this.setState({
                        playerActions: {
                            ...playerActions,
                            [playerID]: actions
                        }
                    }))
            }

        }
        getPlatformStartupData = (profileID) => {
            return profileID && MatchlynxAPI.getPlatformStartupData(profileID)
                .then(({ platforms, players, actions }) => {
                    const activePlayers = players.filter(player => player.platform_id == this.props.platformID)
                    this.setState({ platforms, players, actions, activePlayers })
                });
        }
        componentDidMount = () => {
            // this.getPlatformName(this.props.platformID);
            // this.getPlatformPlayers(this.props.platformID);
            this.getPlatformStartupData(this.props.profileID);
        }
        render() {
            return (
                <WrappedComponent
                    {...this.props}
                    {...this.state}
                    getPlatformName={this.getPlatformName}
                    getUserPlatforms={this.getUserPlatforms}
                    getPlatformPlayers={this.getPlatformPlayers}
                    getPlayerActions={this.getPlayerActions}
                />
            );
        }
    };

    MatchlynxAPIWrapper.displayName = `WithMatchlynx(${WrappedComponent.displayName})`;
    return MatchlynxAPIWrapper;
};

export default WithMatchlynx;