import React, { Component } from 'react';
import AppBar from './AppBar';
import LeftDrawer from './LeftDrawer';
import Dashboard from './Dashboard';
import PlayerEditDialog from './PlayerEditDialog';
import AddActionDialog from './AddActionDialog';
import Paper from 'material-ui/Paper';
class App extends Component {
    constructor() {
        super();
        this.toggleLeftDrawer = this.toggleLeftDrawer.bind(this);
        this.togglePlayerDialog = this.togglePlayerDialog.bind(this);
        this.handleAddActionClose = this.handleAddActionClose.bind(this);
        this.handleAddActionOpen = this.handleAddActionOpen.bind(this);
        this.state = {
            isDrawerOpen: false,
            addAction: false,
            showPlayerDialog: false,
            isGeneral: false
        };
    }
    handleAddActionClose() {
        this.setState({ addAction: false });
    }

    navigateToGeneral = () => {
        console.log('navigateToGeneral', this);
        this.setState(prevState => ({ isGeneral: !prevState.isGeneral }))
    }

    handleAddActionOpen() {
        this.setState({ addAction: true });
    }

    toggleLeftDrawer() {
        this.setState((prevState) => ({ isDrawerOpen: !prevState.isDrawerOpen }));
    }
    togglePlayerDialog() {
        this.setState(prevState => ({ showPlayerDialog: !prevState.showPlayerDialog }));
    }
    render() {
        return (
            <div>
                <AppBar toggleLeftDrawer={this.toggleLeftDrawer} />
                <Paper style={{ height: '100vh' }}>
                    <Dashboard isGeneral={this.state.isGeneral} />
                    <PlayerEditDialog open={this.state.showPlayerDialog} close={this.togglePlayerDialog} />
                </Paper>
                <LeftDrawer
                    {...this.props}
                    navigateToGeneral={this.navigateToGeneral}
                    showDialog={this.togglePlayerDialog}
                    isOpen={this.state.isDrawerOpen}
                    handleAddActionOpen={this.handleAddActionOpen}
                    toggle={this.toggleLeftDrawer} />
                <AddActionDialog open={this.state.addAction} handleAddActionClose={this.handleAddActionClose} />
            </div>
        );
    }
}

export default App;
