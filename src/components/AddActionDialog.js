import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import AddActionSteps from './AddActionSteps';

class AddActionDialog extends Component {
    render() {
        const actions = [
            <FlatButton 
                label="Cancel"
                primary={true}
                onTouchTap={this.props.handleAddActionClose}
            />,
            <FlatButton 
                label="Add Action"
                primary={true}
                onTouchTap={this.props.handleAddActionClose}
            />
        ];

        return (
            <div>
                <Dialog
                    title="Add action"
                    actions={actions}
                    modal={true}
                    open={this.props.open}
                    autoScrollBodyContent={true}
                >
                    <AddActionSteps  />
                </Dialog>
            </div>
        );
    }
}

export default AddActionDialog;