import axios from 'axios';
import data from '../../stubs/frontend-api-data';

const protocol = (isSecure) => (isSecure ? 'https' : 'http');

const MATCHLYNX = `https://www.matchlynx.com`;

const makeURL = (method, action = 'select') => {
    return `${MATCHLYNX}/API/modules/${action}/${method}.php`
};

const requestToAPI = (url, params) => {
    return axios.get(url, { params });
}

export const useVariable = (name) => {
    return `useVariable`
};

export const getPlatformName = (platform_id) => {
    // return new Promise((resolve, reject) => {
    //     setTimeout(_ => resolve('Fooo bar'), 700);
    // })
    const action = "get_platform_name";
    // DataValue.output = "nothing";
    return requestToAPI(makeURL("GetGlobalType"), {
        platform_id,
        action
    }).then(response => response.data);
};

export const getUserPlatforms = (profile_id) => {
    const action = "get_user_platforms";
    return requestToAPI(makeURL("GetGlobalType"), {
        profile_id,
        action
    }).then(response => response.data);
}
export const getPlatformPlayers = (platform_id) => {
    const action = "get_platform_players";
    return requestToAPI(makeURL("GetGlobalType"), {
        platform_id,
        action
    }).then(response => response.data);
}

export const getPlayerActions = (player_id) => {
    const action = "get_player_actions";
    return requestToAPI(makeURL("GetGlobalType"), {
        player_id,
        action
    }).then(response => response.data);
}

export const getPlatformStartupData = (profile_id) => {
    // const startup = data.startup;
    // const platforms = startup.platforms[profile_id];
    // const players = startup.players[startup.current_platform];
    // const playerActions = startup.player_actions[startup.current_platform];
    // const currentPlatform = platforms.find(i => i.id === startup.current_platform);
    // console.log(currentPlatform.text);
    // const responseData = {
    //     platforms,
    //     players,
    //     playerActions,
    //     platformName: currentPlatform.text
    // };
    
    // return new Promise((resolve, reject) => {
    //     setTimeout(_ => resolve(responseData), 800)
    // })
    const action = "get_platform_startup_data";
    return requestToAPI(makeURL("GetGlobalType"), {
        profile_id,
        action
    }).then(response => response.data);
}

export default {
    useVariable,
    getPlatformName,
    getUserPlatforms,
    getPlatformPlayers,
    getPlayerActions,
    getPlatformStartupData
};