import React from 'react';
import axios from 'axios';
const mockedPlatforms = [{
    id: 11,
    name: 'Platform 11',
    players: [{
        id: 1,
        name: '1111'
    }, {
        id: 2,
        name: '2222'
    }]
}, {
    id: 22,
    name: 'Platform 22',
    players: [{
        id: 1,
        name: '34'
    }, {
        id: 2,
        name: '22df22'
    }]
}, {
    id: 33,
    name: 'Platform 33',
    players: [{
        id: 1,
        name: 'sadf'
    }, {
        id: 2,
        name: '34347y7'
    }]
}, {
    id: 44,
    name: 'Platform 44',
    players: [{
        id: 1,
        name: '3445fgfdf'
    }, {
        id: 2,
        name: 'fdfdf'
    }]
}, {
    id: 55,
    name: 'Platform 55',
    players: [{
        id: 1,
        name: '455ggbbbbb'
    }, {
        id: 2,
        name: '222vbvbvbvbvvbbvb2'
    }]
}];

let mockedPlatformIDs = [11, 22, 33, 44, 55];
const MATCHLYNX = 'https://www.matchlynx.com';
const makeURL = (method, action = 'select') => {
    return `${MATCHLYNX}/API/modules/${action}/${method}.php`
};
const requestToAPI = (url, params) => {
    return axios.get(url, { params });
}
const APIWrapper = (Component) => {
    class MatchlynxAPIWrapper extends React.Component {
        constructor() {
            super();
            this.state = {
                userPlatforms: [],
                platformName: 'Matchlynx'
            }
        }
        onChange(changes) {
            this.setState({ ...changes });
        }
        getPlatformName(platformID) {
            return new Promise((resolve, reject) => {
                setTimeout(() => resolve(mockedPlatforms.find(platform => platform.id === platformID)), 500)
            })
            const action = "getPlatformName";
            // DataValue.output = "nothing";
            return requestToAPI(makeURL('GetGlobalType'), {
                platformID,
                action
            }).then(response => response.data);
        }

        getUserPlatforms(profileID) {

            return new Promise((resolve, reject) => {
                setTimeout(() => resolve(mockedPlatformIDs), 500);
            });
        }

        render() {
            return (
                <Component getPlatformName={this.getPlatformName} getUserPlatforms={this.getUserPlatforms} {...this.props} {...this.state} />
            );
        }
    }

    MatchlynxAPIWrapper.displayName = `MatchlynxAPIWrapper(${Component.displayName})`;
    return MatchlynxAPIWrapper;
};

export default APIWrapper;