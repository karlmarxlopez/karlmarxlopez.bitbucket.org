import { orange300 } from 'material-ui/styles/colors';


const orangeTheme = {
    BG: orange300
};
export default orangeTheme;