import React from 'react';
import {
    Card,
    CardHeader,
    CardTitle,
    CardText
} from 'material-ui/Card';

const DashboardPanel = (props) => (
    <Card>
        <CardTitle
            title={props.title}
        />

        <CardText>
            {props.description}
        </CardText>
    </Card>
);

export default DashboardPanel;