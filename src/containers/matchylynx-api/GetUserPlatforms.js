import React from 'react';
import withMatchlynx from './withMatchlynx';
import PropTypes from 'prop-types';

const GetUserPlatforms = (props) => {
    const { profileID, getUserPlatforms, platforms, children, textKey, valueKey } = props;
   
    const childComponents = React.Children.toArray(children)
    if (!profileID) {
        return
    }
    if (!platforms.length) {
        console.log('ear');
        getUserPlatforms(profileID);
    }

    return (
        <div>
            {
                platforms.map((platform, i) => {
                    return React.cloneElement(children, { [textKey]: platform.text, key: i, [valueKey]: platform.id })
                })
            }
        </div>
    );

};

GetUserPlatforms.displayName = 'GetUserPlatforms';

GetUserPlatforms.propTypes = {
    profileID: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};
export default withMatchlynx(GetUserPlatforms);