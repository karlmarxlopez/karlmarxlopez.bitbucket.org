import React from 'react';
import TemplateAPI from '../containers/MatchlynxTemplateAPI';
import renderer from 'react-test-renderer';

describe('MatchlynxTemplateAPI Methods', () => {
    test('useVariable - should return the variable value', () => {
        const Foo = () => (
            <div>
                <h1>
                    Hello
                </h1>
            </div>
        )
        const Wrapped = TemplateAPI(Foo);
        const Blah = <Wrapped/>
        console.log(Foo.props);
        expect(Wrapped.useVariable).toBeTruthy()
    })
})