import React from 'react';
import Subheader from 'material-ui/Subheader';
import DashboardPanel from './DashboardPanel';
import {
    GridList,
    GridTile
} from 'material-ui/GridList';

const dashboardGridData = [{
    title: 'Player Stats',
    description: 'Lorem ipsum dolor sit.',
    img: 'img/graph1.png'
}, {
    title: 'Admin',
    description: 'Lorem ipsum dolor sit amet, consectetur.',
    img: 'img/graph2.png'

}, {
    title: 'Panel 1',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia?',
    img: 'img/graph1.png'

}, {
    title: 'Panel 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia?',
    img: 'img/graph2.png'

}];

function renderIframe() {
    return (
        <iframe src="https://www.matchlynx.com/welcome" frameBorder="0" style={{ padding: '15px', width: '98%', height: '100vh' }}></iframe>
    )
}

const Dashboard = (props) => (
    <div>
        <Subheader>Dashboard</Subheader>
        {props.isGeneral && renderIframe()}
    </div>
);

export default Dashboard;
