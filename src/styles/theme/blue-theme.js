import { lightBlueA400 } from 'material-ui/styles/colors';


const blueTheme = {
    BG: lightBlueA400
};
export default blueTheme;