export default {
    startup: {
        current_platform: 149,
        platforms: {
            1: [
                {
                    "id": 61,
                    "text": "Temp Test",
                    "description": "Test of the system"
                },
                {
                    "id": 135,
                    "text": "FrumBnb",
                    "description": "Kosher accommodations in Frum homes"
                },
                {
                    "id": 139,
                    "text": "Chai",
                    "description": "A network of community leaders throughout Africa, devoted to building African communities and connecting them internationally."
                },
                {
                    "id": 142,
                    "text": "TestFoo",
                    "description": "Test"
                },
                {
                    "id": 145,
                    "text": "Luchos",
                    "description": "A community organizer for Jewish minions"
                },
                {
                    "id": 149,
                    "text": "LuchotZemanim",
                    "description": "A Jewish community building tool, allowing organizers to show prayer services and other relevant information for a city or neighborhood"
                }
            ]
        },
        players: {
            61: [{
                "id": 11,
                "player_name": "Organizer"
            }, {
                "id": 22,
                "player_name": "Location Rep"
            }, {
                "id": 33,
                "player_name": "Davin"
            }],
            135: [{
                "id": 22,
                "player_name": "Organizer"
            }, {
                "id": 33,
                "player_name": "Location Rep"
            }, {
                "id": 44,
                "player_name": "Davin"
            }],
            139: [{
                "id": 33,
                "player_name": "Organizer"
            }, {
                "id": 44,
                "player_name": "Location Rep"
            }, {
                "id": 55,
                "player_name": "Davin"
            }],
            142: [{
                "id": 44,
                "player_name": "Organizer"
            }, {
                "id": 55,
                "player_name": "Location Rep"
            }, {
                "id": 66,
                "player_name": "Davin"
            }],
            145: [{
                "id": 77,
                "player_name": "Organizer"
            }, {
                "id": 88,
                "player_name": "Location Rep"
            }, {
                "id": 99,
                "player_name": "Davin"
            }],
            149: [{
                "id": 157,
                "player_name": "Organizer"
            }, {
                "id": 158,
                "player_name": "Location Rep"
            }, {
                "id": 159,
                "player_name": "Davin"
            }]
        },
        player_actions: {
            149: {
                157: [{
                    id: 1,
                    name: "Organizer action 1"
                }, {
                    id: 2,
                    name: "Organizer action 2"
                }],
                158: [{
                    id: 1,
                    name: "Hire"
                }, {
                    id: 2,
                    name: "Manage pages"
                }, {
                    id: 3,
                    name: "Delete users"
                }],
                159: [{
                    id: 1,
                    name: "Post"
                }, {
                    id: 2,
                    name: "Send Email"
                }, {
                    id: 3,
                    name: "Sleep"
                }]
            }
        }
    },
    variables: {
        Logo: 'My shiny Logo',
        Name: 'Sample name variable',
        Community: 'Awesome Community',
        Map: 'Israel Map',
        Season: 'Christmas',
        serviceTypeArea: 'Lalala',
        serviceType: [
            {
                name: 'Service type 1'
            }, {
                name: 'Service type 2'
            }, {
                name: 'Service type 3'
            }
        ]
    },
    currentCommunity: 'Awesome Community',
    currentSeason: 'Christmas'
}