import React from 'react';
import { render } from 'react-dom';
import HelloWorld from '../HelloWorld';
import ShallowRenderer from 'react-test-renderer/shallow';

test('Should render without crashing', () => {
    const div = document.createElement('div');
    render(<HelloWorld name="beepboop"/>, div);
});

test('Render the right text.', () => {
    const renderer = new ShallowRenderer();
    renderer.render(<HelloWorld name="Boop" />);
    const result = renderer.getRenderOutput();
    expect(result.type).toBe('div');
    expect(result.props.children).toEqual(<h1>Boop</h1>)
});

test('Should be a pure function', () => {
    const addOne = (n) => n + 1;
    let n = 0;
    addOne(n);
    addOne(n);
    n = addOne(n);
    expect(n).toEqual(1);
});