import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import PlayerEditSteps from './PlayerEditSteps';

const buttons = [
    <FlatButton label="Cancel"/>,
    <FlatButton label="Update"/>
];

class PlayerEditDialog extends React.Component {
    constructor() {
        super();
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.renderStepActions = this.renderStepActions.bind(this);
        this.state= {
            stepIndex: 0,
            editingFinished: false
        };
    }

    handleNext() {
        const { stepIndex } = this.state;

        this.setState({
            stepIndex: stepIndex + 1,
            editingFinished: stepIndex >= 2
        });
    }

    handlePrev() {
        const { stepIndex } = this.state;
        if(stepIndex > 0) {
            this.setState({
                stepIndex: stepIndex - 1
            });
        } 
    }
    renderStepActions(step) {
    const {stepIndex} = this.state;

        return (
            <div style={{margin: '12px 0'}}>
                <RaisedButton
                  label={stepIndex === 2 ? 'Finish' : 'Next'}
                  disableTouchRipple={true}
                  disableFocusRipple={true}
                  primary={true}
                  onTouchTap={this.handleNext}
                  style={{marginRight: 12}}
                />
                {step > 0 && (
                  <FlatButton
                    label="Back"
                    disabled={stepIndex === 0}
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    onTouchTap={this.handlePrev}
                  />
                )}
            </div>
        );
    }
    render() {
        return (
            <Dialog
                title="Edit this player"
                actions={[
                    <FlatButton onTouchTap={this.props.close} label="Cancel"/>,
                    <FlatButton onTouchTap={this.props.close} label="Update"/>
                ]}
                open={this.props.open}
                onRequestClose={this.props.close}
            >
                <PlayerEditSteps 
                    handleNext={this.handleNext} 
                    handlePrev={this.handlePrev}
                    renderStepActions={this.renderStepActions}
                    {...this.state}
                />
            </Dialog>
        );
    }
}

export default PlayerEditDialog;