import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';

// SVG Icons
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import AddIcon from 'material-ui/svg-icons/content/add'
import {
    List,
    ListItem,
    makeSelectable
} from 'material-ui/List';
import { grey400, darkBlack, lightBlack } from 'material-ui/styles/colors';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MenuItem from 'material-ui/MenuItem';

import GetPlatformName from '../containers/matchylynx-api/GetPlatformName';
import { getUserPlatforms } from '../containers/matchylynx-api/APIMethods';

const iconButtonElement = (
    <IconButton
        touch={true}
        tooltip="Settings"
        tooltipPosition="bottom-left"
    >
        <SettingsIcon color={grey400} />
    </IconButton>
);

const rightIconMenu = (
    <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem>Profile info</MenuItem>
        <MenuItem>Offerings</MenuItem>
        <MenuItem>Delete</MenuItem>
    </IconMenu>
);

const platformIconMenu = (props, setState) => (
    <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem>Community Info Types</MenuItem>
        <MenuItem>General</MenuItem>
        <MenuItem onTouchTap={() => setState({ platformName: props.getPlatformName() })}>Change Platform Name</MenuItem>
        <MenuItem>Advanced</MenuItem>
    </IconMenu>
);


class ModeratorMenu extends Component {
    constructor() {
        super();
        this.handleAPICall = this.handleAPICall.bind(this);
        this.state = {
            platformName: "Luchos Zemanim App",
            selectPlatform: false,
            selectedPlatform: 149,
            temp: 149,
            platforms: [],
            players: []
        }
        this.handleClose = this.handleClose.bind(this);
        this.changePlatform = this.changePlatform.bind(this);
        this.showPlatformName = this.showPlatformName.bind(this);
    }
    handleAPICall() {
        this.props.getPlatformName(149).then(response => this.setState({ platformName: response }))
    }
    getPlatformName = () => {
        return this.props.platforms.reduce((name, platform) => {
            if (platform.id == this.state.selectedPlatform) {
                name = platform.platform_name;
            }
            return name;
        }, "");
    }
    componentDidMount() {
        getUserPlatforms(1)
            .then(platforms => this.setState({ platforms }))
    }

    handleClose() {
        this.setState({
            selectPlatform: false,

        });
    }

    changePlatform() {
        console.log('Change platform');
        this.setState({
            selectPlatform: false,
            selectedPlatform: this.state.temp,

        });
        this.props.getPlatformPlayers(this.state.temp);
    }
    showPlatformName() {
        const { selectedPlatform, platformName } = this.state;

        !selectedPlatform ? platformName : this.props.getPlatformName(selectedPlatform).then(platform => this.setState({ platformName: platform.name, players: platform.players }))
        return this.state.platformName;
    }
    renderPlayers() {
        const { activePlayers } = this.props
        if (activePlayers.length) {
            return activePlayers.map(({ player_id, player_name }, i) => (
                <ListItem
                    key={`player-${player_id}`}
                    primaryTogglesNestedList
                    insetChildren
                    primaryText={player_name}
                    rightIconButton={rightIconMenu}
                    leftAvatar={<Avatar src="https://api.adorable.io/avatars/111/dfddd3.png" />}
                    onNestedListToggle={() => this.props.getPlayerActions(player_id)}
                    nestedItems={[
                        ...this.renderActions(player_id),
                        <ListItem onTouchTap={this.props.handleAddActionOpen} key="add-action" primaryText="Add Action" leftIcon={<AddIcon />} />
                    ]}>
                </ListItem>
            ))
        }
    }

    renderActions(playerID) {
        const actions = this.props.actions[playerID]
        if (actions && actions.length) {
            return actions.map((action, i) => (
                <ListItem key={i} primaryText={`${action.name}`}></ListItem>
            ))
        } else {
            return [];
        }
    }
    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Change"
                primary={true}
                onTouchTap={this.changePlatform}
            />,
        ];
        return (
            <div>
                <Dialog
                    style={{ width: '450px' }}
                    title="Choose platform"
                    actions={actions}
                    modal={true}
                    open={this.state.selectPlatform}>
                    <SelectField
                        floatingLabelText="Platforms"
                        value={this.state.temp}
                        onChange={(e, index, value) => {


                            this.setState({ temp: value })
                        }}>
                        <MenuItem value={null} primaryText=" " />
                        {
                            this.props.platforms.map(({ platform_id, platform_name }) => <MenuItem key={platform_id} value={platform_id} primaryText={platform_name} />)

                        }
                    </SelectField>
                </Dialog>
                <List>

                    <ListItem primaryText={this.getPlatformName()} rightIconButton={
                        <IconMenu iconButtonElement={iconButtonElement}>
                            <MenuItem>Community Info Types</MenuItem>
                            {/* <MenuItem>General</MenuItem> */}
                            {/*<MenuItem onTouchTap={this.handleAPICall}>Change Platform Name</MenuItem>*/}
                            <MenuItem onTouchTap={() => this.setState({ selectPlatform: true })}>Select Platform</MenuItem>
                            <MenuItem>Advanced</MenuItem>
                        </IconMenu>
                    }></ListItem>

                    <ListItem

                        value={1}
                        primaryText="General Pages"
                        leftAvatar={<Avatar src="https://api.adorable.io/avatars/285/matchy@foo.bar.png" />}
                        nestedItems={[
                            <ListItem key="landing-page" primaryText="Landing Page" onTouchTap={_ => this.props.navigateToGeneral()} />
                        ]}
                    />

                    {
                        this.renderPlayers()
                    }

                    <ListItem key="add-player" primaryText="Add new player" leftIcon={<AddIcon />} />
                </List>
            </div>
        );
    }
};

export default ModeratorMenu;