import React from 'react';
import withMatchlynx from './withMatchlynx';
import PropTypes from 'prop-types';
let lastID = null;
const GetPlatformName = (props) => {
    const { platformID, getPlatformName, platformName, platforms, children } = props;
    const childComponents = React.Children.toArray(children)
    if (!platformID) {
        return
    }
    if (lastID !== platformID) {
        console.log('GetPlatformName ', platformID);
        lastID = platformID
        getPlatformName(platformID);
    }


    if (childComponents.length) {
        return (
            <div>
                {
                    childComponents.map(child => React.cloneElement(child, { platformName }))
                }
            </div>
        );
    } else {
        return (
            <span>{platformName}</span>
        );
    }

};

GetPlatformName.displayName = 'GetPlatformName';

GetPlatformName.propTypes = {
    platformID: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};
export default withMatchlynx(GetPlatformName);