var global_path_url = $("#RootPath").val();


//return the newly added global entity type
function createGlobalType(typeName, valueType){
	var DataValue = {};
	DataValue.typeName = typeName;
	DataValue.valueType = valueType;
	DataValue.action = "CreateGlobalType";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

// return boolean true or false. if true success if false it failed
function UpdateGlobalTypeText(GlobalId, NewValue){
	var DataValue = {};
	DataValue.GlobalId = GlobalId;
	DataValue.NewValue = NewValue;
	DataValue.action = "UpdateGlobalTypeText";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

//remove global type
//return success or failed msg
function DeleteGlobalType(GlobalId){
	var DataValue = {};
	DataValue.GlobalId = GlobalId;
	DataValue.action = "DeleteGlobalType";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

//return global entity type id
function textToGlobalID(GlobalText){
	var DataValue = {};
	DataValue.GlobalText = GlobalText;
	DataValue.action = "GetIdOfGlobalTypeUsingGlobalText";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/select/GetLocalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}
// return an local Id
function TextToLocalID(TypeName, PlatformId){
	var DataValue = {};
	DataValue.TypeName = TypeName;
	DataValue.PlatformId = PlatformId;
	DataValue.action = "TextToLocalID";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/select/GetLocalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}
//return a txt value
function GlobalIDToText(GlobalId){
	var DataValue = {};
	DataValue.GlobalId = GlobalId;
	DataValue.action = "GlobalIDToText";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/select/GetLocalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}
//return a text value
function getTypeName(LocalEntityTypeId){
	var DataValue = {};
	DataValue.LocalEntityTypeId = LocalEntityTypeId;
	DataValue.action = "getTypeName";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/select/GetLocalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

//return boolean
function CreateInstance(localEntityTypeID, NewValue, Parent, ProfileId=0){
	var DataValue = {};
	DataValue.localEntityTypeID = localEntityTypeID;
	DataValue.NewValue = NewValue;
	DataValue.Parent = Parent;
	DataValue.ProfileId = ProfileId;
	DataValue.action = "CreateInstance";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

//return a boolean
function UpdateInstance(LocalEntityTypeID, InstanceID, NewValue){
	var DataValue = {};
	DataValue.LocalEntityTypeID = LocalEntityTypeID;
	DataValue.InstanceID = InstanceID;
	DataValue.NewValue = NewValue;
	DataValue.action = "UpdateInstance";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function DeleteInstance(localEntityTypeID, InstanceID){
	var DataValue = {};
	DataValue.LocalEntityTypeID = LocalEntityTypeID;
	DataValue.InstanceID = InstanceID;
	DataValue.action = "DeleteInstance";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function UpdateLocalType(LocalId, NewValue){
	var DataValue = {};
	DataValue.LocalId = LocalId;
	DataValue.NewValue = NewValue;
	DataValue.action = "UpdateLocalType";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function DeleteLocalType(LocalId){
	var DataValue = {};
	DataValue.LocalId = LocalId;
	DataValue.action = "DeleteLocalType";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function CreateGlocalAssn(PrimaryId, SecondaryId){
	var DataValue = {};
	DataValue.PrimaryId = PrimaryId;
	DataValue.SecondaryId = SecondaryId;
	DataValue.action = "CreateGlocalAssn";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function DeleteGlocalAssn(GlobalAssnID){
	var DataValue = {};
	DataValue.PrimaryId = PrimaryId;
	DataValue.SecondaryId = SecondaryId;
	DataValue.action = "DeleteGlocalAssn";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function UserProposedInstance(localEntityTypeID, newValue, Parent=null){
	var DataValue = {};
	DataValue.localEntityTypeID = localEntityTypeID;
	DataValue.newValue = newValue;
	DataValue.Parent = Parent;
	DataValue.action = "UserProposedInstance";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function CreateInstanceAssn(PrimaryGlobalID, PrimaryID, SecondaryGlobalID, SecondaryID){
	var DataValue = {};
	DataValue.PrimaryGlobalID = PrimaryGlobalID;
	DataValue.SecondaryGlobalID = SecondaryGlobalID;
	DataValue.PrimaryID = PrimaryID;
	DataValue.SecondaryID = SecondaryID;
	DataValue.action = "CreateInstanceAssn";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function deleteLocalAssn($LocalAssn){
	var DataValue = {};
	DataValue.LocalAssn = LocalAssn;
	DataValue.action = "deleteLocalAssn";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function addChild(localParentID, localChildID, treeID){
	var DataValue = {};
	DataValue.localParentID = localParentID;
	DataValue.localChildID = localChildID;
	DataValue.treeID = treeID;
	DataValue.action = "addChild";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function createTypeAdmin(parentLocalID, globalID, platformID){
	var DataValue = {};
	DataValue.parentLocalID = parentLocalID;
	DataValue.globalID = globalID;
	DataValue.platformID = platformID;
	DataValue.action = "createTypeAdmin";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function newPlatform(platformName){
	var DataValue = {};
	DataValue.platformName = platformName;
	DataValue.action = "newPlatform";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getPlatformName(platformID){
	var DataValue = {};
	DataValue.platformID = platformID;
	DataValue.action = "getPlatformName";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function setPlatformName(platformId, newPlatformName){
	var DataValue = {};
	DataValue.platformId = platformId;
	DataValue.newPlatformName = newPlatformName;
	DataValue.action = "setPlatformName";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function deleteplatform(platformId){
	var DataValue = {};
	DataValue.platformId = platformId;
	DataValue.action = "deleteplatform";
	DataValue.output = "nothing";
	$.ajax({
	    url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getPlatformPlayers(platformID){
	var DataValue = {};
	DataValue.platformId = platformId;
	DataValue.action = "deleteplatform";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getPlayerName(playerId){
	var DataValue = {};
	DataValue.playerId = playerId;
	DataValue.action = "getPlayerName";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function newPlayer(platformId, name){
	var DataValue = {};
	DataValue.platformId = platformId;
	DataValue.name = name;
	DataValue.action = "newPlayer";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function setPlayerName(playerID, name){
	var DataValue = {};
	DataValue.playerID = playerID;
	DataValue.name = name;
	DataValue.action = "setPlayerName";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function deletePlayer(playerID){
	var DataValue = {};
	DataValue.playerID = playerID;
	DataValue.action = "deletePlayer";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getPlayerActions(playerID){
	var DataValue = {};
	DataValue.playerID = playerID;
	DataValue.action = "getPlayerActions";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function newAction(playerID, name){
	var DataValue = {};
	DataValue.playerID = playerID;
	DataValue.name = name;
	DataValue.action = "newAction";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getActionName(playerID){
	var DataValue = {};
	DataValue.playerID = playerID;
	DataValue.action = "getActionName";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function setActionName(actionID, name){
	var DataValue = {};
	DataValue.actionID = actionID;
	DataValue.name = name;
	DataValue.action = "setActionName";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function deleteAction(actionID){
	var DataValue = {};
	DataValue.actionID = actionID;
	DataValue.action = "deleteAction";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getActionInfo(actionID){
	var DataValue = {};
	DataValue.actionID = actionID;
	DataValue.action = "getActionInfo";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function getUserPlatforms(ProfileId){
	var DataValue = {};
	DataValue.ProfileId = ProfileId;
	DataValue.action = "getUserPlatforms";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/select/GetGlobalType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function createTypeTree(platformID, infoType, playerID){
	var DataValue = {};
	DataValue.platformID = platformID;
	DataValue.infoType = infoType;
	DataValue.playerID = playerID;
	DataValue.action = "createTypeTree";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/add/CreateType.php",
	    type: "GET",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}

function deactivateUser(platformId){
	var DataValue = {};
	DataValue.platformId = platformId;
	DataValue.action = "deactivateUser";
	DataValue.output = "nothing";
	$.ajax({
	   	url: global_path_url+"API/modules/update/UpdateGlobalTypes.php",
	    type: "POST",
	    async: false,
	    data: DataValue,
	    success: function(response) {
	      DataValue.output = response;
	    },
	    error: function (jqXHR, status, err) {
	      DataValue.output=[];
	    }
  	});
	while(DataValue.output === "nothing"){
	        
	}
	return DataValue.output;
}