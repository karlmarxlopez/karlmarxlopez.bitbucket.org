import React, { Component } from 'react'
import Helmet from 'react-helmet';
import data from '../stubs/frontend-api-data';

const MatchlynxTemplateAPIWrapper = (ComponentToWrap) => {
    class MatchlynxTemplateAPI extends Component {
        constructor() {
            super()
            this.state = {
                isDefault: true
            }
        }
        useVariable(name, isDefault) {
            let __default = `This is ${name} default value`;

            return isDefault ? __default : data.variables[name];
        }

        dataField(variableName) {
            return this.useVariable(variableName)
        }

        render() {
            return (
                <div>
                    <ComponentToWrap 
                    {...this.props} 
                    {...this.state} 
                    useVariable={this.useVariable}
                    />
                </div>

            );
        }
    }

    MatchlynxTemplateAPI.displayName = `MatchlynxTemplateAPI(${ComponentToWrap.displayName})`;
    return MatchlynxTemplateAPI;
}

export default MatchlynxTemplateAPIWrapper;