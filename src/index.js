import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import tapEventPlugin from 'react-tap-event-plugin';

import App from './components';
import AppBar from './components/AppBar';
import MatchlynxAPI from './containers/MatchlynxAPI'
import SearchResults from './containers/SearchResults'
import MatchlynxTemplateAPI from './containers/MatchlynxTemplateAPI';
import DocumentTitle from './components/DocumentTitle';

import GetPlatformName from './containers/matchylynx-api/GetPlatformName';
import withMatchlynx from './containers/matchylynx-api/withMatchlynx';
tapEventPlugin();

const Header = (props) => (
    <div>
        {React.createElement(props.size, null, props.text)}
    </div>
)

const Image = (props) => (
    <div>
        <img src={props.src} />
    </div>
)


const MyComponent = (props) => (
    <div>
        <DocumentTitle {...props} />
        <AppBar {...props} />
        <SearchResults groupingTypes={null} constraints={[
            {
                variable: "Community",
                operator: "==",
                compareTo: "currentCommunity"
            },
            {
                variable: "Season",
                operator: "==",
                compareTo: "currentSeason"
            }
        ]}>
            <Header
                size="h1"
                text={props.isDefault ? props.useVariable("Community", props.isDefault) : `${props.useVariable("Community")} Prayer times for ${props.useVariable("Season")}`}
            />
            <SearchResults variableName="name" groupingTypes={props.useVariable("serviceType")} {...props}>
                <Header
                    size="h4"
                    text={props.isDefault ? props.useVariable("serviceType", props.isDefault) : props.serviceType} />
            </SearchResults>
        </SearchResults>
    </div>)
MyComponent.displayName = "MyComponent"
const Wrapped = MatchlynxTemplateAPI(MyComponent);


const Matchlynx = withMatchlynx(App);
ReactDOM.render(<MuiThemeProvider>
    <div>
        <Matchlynx platformID={149} profileID={1}/>
    </div>
</MuiThemeProvider>, document.getElementById('root'));
// const ZX = (props) => {


//     return (
//         <div>
//             <h1>{props.platformName}</h1>
//         </div>
//     );
// };
// const ML = withMatchlynx(ZX);
// ReactDOM.render(<div>
//     <GetPlatformName platformID="149"></GetPlatformName>
//     <GetPlatformName platformID="149"><ZX /><ZX /></GetPlatformName>
// </div>, document.getElementById('root'));
// ReactDOM.render(<App/>, document.getElementById('root'))
registerServiceWorker();