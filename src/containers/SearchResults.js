import React, { Component } from 'react'
import data from '../stubs/frontend-api-data'

const operators = {
    "==": (needle, haystack) => {
        return data.variables[needle] === data[haystack]
    }
}


class SearchResults extends Component {
    executeConstraints() {
        const { constraints } = this.props;
        if(!constraints) {
            return null
        }
        return constraints.reduce((results, constraint) => {
            let operator = operators[constraint.operator]
            if (operator && operator(constraint.variable, constraint.compareTo)) {
                console.log(data);
                results = {
                    ...results,
                    [constraint.variable]: data.variables[constraint.variable]
                }
            }

            return results
        }, {})
    }
    renderChildren() {
        const { constraints, groupingTypes, isDefault, variableName } = this.props
        if (!isDefault && groupingTypes && groupingTypes.length) {
            console.log('Has grouping type');
            // return groupingTypes.map((type) => {
            //     return React.cloneElement(this.props.children, {...this.props, serviceType: type.name})
            // })
            // const mapped = React.Children.map(this.props.children, child => {
            //     React.cloneElement(child, { ...this.props })
            // })
            return groupingTypes.map((groupingType, index) => {
                return React.cloneElement(this.props.children, { key: index, text: groupingType[variableName] })
            })

        }
        return React.Children.toArray(this.props.children).reduce((children, child, i) => {
            if (child.type.name !== "SearchResults") {
                children = children.concat(React.cloneElement(child, { ...this.props, key: i }))
            } else {
                children = children.concat(React.cloneElement(child, null))
            }
            return children
        }, [])

    }
    render() {
        return (
            <div>
                {this.renderChildren()}
            </div>
        );
    }
}

export default SearchResults;