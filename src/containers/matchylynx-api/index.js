import withMatchlynx from './withMatchlynx';
import GetPlatformName from './GetPlatformName';


export default {
    withMatchlynx,
    GetPlatformName
};