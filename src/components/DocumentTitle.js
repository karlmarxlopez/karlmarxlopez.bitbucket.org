import React from 'react'
import { Helmet } from 'react-helmet'

const DocumentTitle = (props) => (
    <Helmet>
        <title>{props.useVariable('Logo', props.isDefault)}</title>
    </Helmet>
)


export default DocumentTitle