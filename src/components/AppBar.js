import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import theme from '../styles';
import Avatar from 'material-ui/Avatar';
const rightElements = (
    <FlatButton label="Account Settings" />
);
/*title={props.useVariable("Name", props.isDefault)}*/
const MLAppBar = (props) => (
    <AppBar
        title="Matchlynx"
        iconElementRight={rightElements}
        onLeftIconButtonTouchTap={props.toggleLeftDrawer}
        style={{ backgroundColor: theme.BG }}
    >
        {React.Children.map(props.children, child => (
            React.cloneElement(child, props)
        ))}
    </AppBar>
);

export default MLAppBar;