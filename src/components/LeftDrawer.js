import React from 'react';
import Drawer from 'material-ui/Drawer';
import ModeratorMenu from './ModeratorMenu';

const LeftDrawer = (props) => (
    <Drawer 
        docked={false}
        onRequestChange={props.toggle}
        open={props.isOpen}
    >
        <ModeratorMenu {...props}/>
    </Drawer>
);

export default LeftDrawer;