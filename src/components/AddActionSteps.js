import React from 'react';
import {
    Step,
    Stepper,
    StepButton,
    StepContent,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton'
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Subheader from 'material-ui/Subheader';
const steps = [{
    label: 'Name',
    description: 'Add the name for this action',
    input: {
        type: 'text',
        placeholder: 'The name for this action'
    }
}, {
    label: 'Info type',
    description: 'What kind of info will the organization use for?',
    input: {
        type: 'radio',
        choices: [
            {
                text: 'Profile',
                description: 'Something done to an Organizer',
                value: 'info-type-profile'
            }, {
                text: 'Community',
                description: 'Offers or requests between Organizer',
                value: 'info-type-community'
            }, {
                text: 'Offering',
                description: 'User Content or Data. Includes posts, files, etc.',
                value: 'info-type-offering'
            }
        ]
    }
}, {
    label: 'Recipient',
    description: 'Which of this is the recipient?',
    input: {
        type: 'radio',
        choices: [{
            text: 'Organizer',
            value: 'recipient-organizer',
        },
        {
            text: 'Location Rep',
            value: 'recipient-location-rep',
        },
        {
            text: 'Participant',
            value: 'recipient-participant',
        }]
    }
}, {
    label: 'Action',
    description: 'Which action will the Organizer do ?',
    input: {
        type: 'radio',
        choices: [{
            text: 'Choice 1',
            value: 'action-choice-1'
        }, {
            text: 'Choice 1',
            value: 'action-choice-1'
        }]
    }
}, {
    label: 'Selection',
    description: 'How is the proper Organizer found for the action ?',
    input: {
        type: 'radio',
        choices: [{
            text: 'Searching',
            description: 'The Organizer selects the false via search results',
            value: 'selection-searching'
        }, {
            text: 'Identification',
            description: 'The Organizer provides a number or other identification to determine the false',
            value: 'selection-identification'
        }, {
            text: 'Automation',
            description: 'Determination of the false is automatic',
            value: 'selection-automation'
        }]
    }
}, {
    label: 'Choose template',
    description: '',
    input: {
        type: 'select',
        label: 'Templates',
        options: [{
            text: 'Basic',
            value: 'basic-template'
        }, {
            text: 'Blog',
            value: 'blog-template'
        }]
    }
}]
/**
 * A basic vertical non-linear implementation
 */
class VerticalNonLinear extends React.Component {

    constructor() {
        super();
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.state = {
            stepIndex: 0
        };
    }
    handleNext = () => {
        const { stepIndex } = this.state;
        if (stepIndex < (steps.length - 1)) {
            this.setState({ stepIndex: stepIndex + 1 });
        }
    };

    handlePrev = () => {
        const { stepIndex } = this.state;
        if (stepIndex > 0) {
            this.setState({ stepIndex: stepIndex - 1 });
        }
    };

    renderStepActions(step) {
        return (
            <div style={{ margin: '12px 0' }}>
                <RaisedButton
                    label={step === (steps.length - 1) ? 'Finish' : 'Next'}
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    primary={true}
                    onTouchTap={this.handleNext}
                    style={{ marginRight: 12 }}
                />
                {step > 0 && (
                    <FlatButton
                        label="Back"
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        onTouchTap={this.handlePrev}
                    />
                )}
            </div>
        );
    }

    renderStepDescription(description) {
        return description ? <p>{description}</p> : '';
    }

    renderStepInput(input) {
        switch (input.type) {
            case 'text':
                return (
                    <TextField hintText={input.placeholder} />
                );
            case 'radio':
                return (
                    <RadioButtonGroup name={new Date().getTime().toString()}>
                        {
                            input.choices.map((choice, index) => (
                                <RadioButton
                                    key={index}
                                    value={choice.value}
                                    label={`${choice.text}${choice.description ? `(${choice.description}` : ''}`}

                                />
                            ))
                        }
                    </RadioButtonGroup>
                );

            case 'select':
                return (
                    <SelectField 
                        floatingLabelText={input.label} 
                        autoWidth={true}
                    >
                        {
                            input.options.map((option, i) => (
                                <MenuItem
                                    value={option.value}
                                    key={i}
                                    primaryText={option.text}
                                ></MenuItem>
                            ))
                        }
                    </SelectField>
                );
            default:
        }
    }

    renderSteps() {
        return steps.map((step, i) => {
            return (
                <Step key={i}>
                    <StepButton onTouchTap={() => this.setState({ stepIndex: i })}>
                        {step.label}
                    </StepButton>
                    <StepContent>
                        {this.renderStepDescription(step.description)}
                        {this.renderStepInput(step.input)}
                        {this.renderStepActions(i)}
                    </StepContent>
                </Step>
            );
        });
    }

    render() {
        const { stepIndex } = this.state;

        return (

            <Stepper
                activeStep={stepIndex}
                linear={false}
                orientation="vertical"
            >
                {this.renderSteps()}
            </Stepper>

        );
    }
}

export default VerticalNonLinear;